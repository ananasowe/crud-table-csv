import csv
import tkinter.filedialog as fd
import utils.tables


def set_sheet_data(context, csv_data):
    context.csv_data = csv_data
    context.sheet.set_sheet_data(data=csv_data,
                                 reset_col_positions=True,
                                 reset_row_positions=True,
                                 redraw=True,
                                 verify=False,
                                 reset_highlights=False)


def toggle_header(context):
    enable = context.builder.get_variable('first_row_as_column_names').get()
    context.first_row_is_headers = enable
    print("Toggling header enabled to: ", enable)
    if len(context.sheet.data) > 0:
        set_headers(context, enable=enable)
    context.sheet.redraw()
    return enable


def set_headers(context, enable=False):
    if enable:
        context.swapped_headers = context.csv_data.pop(0)
        context.sheet.headers(newheaders=context.swapped_headers, redraw=False)
        set_sheet_data(context, context.csv_data)
    else:
        context.csv_data.insert(0, context.swapped_headers)
        context.sheet.headers(newheaders=[], redraw=False)
        set_sheet_data(context, context.csv_data)


def load_csv(context):
    file_path = fd.askopenfilename(filetypes=[("Pliki CSV", '*.csv')],
                                   title="Otwórz plik CSV")
    if file_path:
        with open(file_path, 'r') as csv_file:
            csv_read = csv.reader(csv_file, delimiter=',')
            csv_data = []
            for row in csv_read:
                csv_data.append(row)
            csv_data = utils.tables.convert_numbers_in_table(csv_data)
            set_sheet_data(context, csv_data)
            set_headers(context, enable=context.first_row_is_headers)
        return file_path


def write_table_to_csv(table, file_path):
    file_path = fd.asksaveasfilename(defaultextension=".csv",
                                     filetypes=[("Pliki CSV", "*.csv")],
                                     title="Zapisz plik CSV") if not file_path else file_path

    if not file_path:
        print("Anulowano zapisywanie pliku")
        return

    try:
        with open(file_path, mode='w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerows(table)
        print(f"Zapisano plik CSV: {file_path}")
        return file_path
    except Exception as e:
        print(f"Błąd zapisywania pliku CSV: {e}")
    return None
