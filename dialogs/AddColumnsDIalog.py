from tkinter import END
import pygubu
from static_variables import PROJECT_UI


class ColumnsDialog:
    def __init__(self, master=None):
        self.column_names = []
        self.builder = builder = pygubu.Builder()
        builder.add_from_file(PROJECT_UI)
        self.mainwindow = builder.get_object('toplevel_dialog_add_fields', master)
        self.mainwindow.geometry("300x300")
        self.mainwindow.protocol("WM_DELETE_WINDOW", self.on_close_window)
        self.mainwindow.grab_set() # Zapobiega modyfikacji danych w oknie edycji podczas dodawnia pól
        self.listbox_fields_to_add = self.builder.get_object('editable_listbox_fields_to_add')
        builder.connect_callbacks(self)

    def on_spinbox_num_of_fields(self):
        self.column_names = list(self.listbox_fields_to_add.get_values())
        desired_num_of_fields = self.builder.get_variable('num_of_fields').get()
        while len(self.column_names) > desired_num_of_fields:
            self.column_names.pop()
        while desired_num_of_fields > len(self.column_names):
            self.column_names.append("Kolumna " + str(len(self.column_names) + 1))
        self.listbox_fields_to_add.set_values(self.column_names)

    def on_button_add_fields_accept(self):
        self.column_names = list(self.listbox_fields_to_add.get_values())
        self.mainwindow.destroy()

    def close_popup(self):
        self.column_names = []
        print("Row creation cancelled")
        self.mainwindow.destroy()

    def on_button_add_fields_cancel(self):
        self.close_popup()

    def on_close_window(self):
        self.close_popup()

    def run_waiting(self):
        self.mainwindow.wait_window()
        return self.column_names
