import pygubu
from static_variables import PROJECT_UI



class FieldSelectDialog:
    def __init__(self, master=None, columns=None):
        if columns is None:
            columns = []
        self.field_name = ""
        self.field_value = ""
        self.response = ()
        self.builder = builder = pygubu.Builder()
        builder.add_from_file(PROJECT_UI)
        self.mainwindow = builder.get_object('toplevel_dialog_field_select', master)
        self.column_selection = builder.get_object('optionmenu_field')
        self.column_selection.set_menu(columns[0], *columns)
        self.mainwindow.protocol("WM_DELETE_WINDOW", self.on_close_window)
        self.mainwindow.grab_set()
        builder.connect_callbacks(self)

    def on_close_window(self, *args):
        self.response = ()
        self.mainwindow.destroy()

    def on_button_field_cancel(self):
        self.response = ()
        self.mainwindow.destroy()

    def on_button_field_accept(self):
        self.field_name = self.builder.get_variable('optionmenu_field_selection').get()
        self.field_value = self.builder.get_variable('entry_field_value').get()
        self.response = (self.field_name, self.field_value)
        self.mainwindow.destroy()

    def run_waiting(self):
        self.mainwindow.wait_window()
        return self.response
