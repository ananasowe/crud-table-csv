import tkinter as tk
import tkinter.ttk as ttk
from tkinter import END
from .EditableListbox import EditableListbox


class EditableListboxWidget(ttk.Frame):
    """Editable listbox widget"""

    def __init__(self, master=None, **kw):
        ttk.Frame.__init__(self, master, **kw)
        # subwidgets
        self.editable_listbox = o = EditableListbox(self)
        o.pack(expand=True, fill='both')
        self.columnconfigure(0, weight=1)

    def set_values(self, values):
        self.editable_listbox.delete(0, self.editable_listbox.size())
        for value in values:
            self.insert_value(value)

    def insert_value(self, value):
        self.editable_listbox.insert(END, value)

    def get_values(self):
        return self.editable_listbox.get(0, self.editable_listbox.size())


if __name__ == "__main__":
    root = tk.Tk()
    editable_listbox = EditableListboxWidget(root)
    editable_listbox.grid()
    editable_listbox.set_values(["Test 1", "Test 2"])

    root.mainloop()
