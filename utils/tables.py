def sort_table_by_column(table, column, reverse=False):
    """
    Sorts a table by the specified column. Automatically handles numerical and string data.

    Parameters:
    - table (list): The input table, where each element is a list representing a row.
    - column (int): The column index to sort the table by.

    Returns:
    - list: The sorted table.
    """
    # Check if the column contains numerical data
    is_numeric = all(isinstance(row[column], (int, float)) for row in table)

    return sorted(table, key=lambda x: (isinstance(x[column], str), x[column]), reverse=reverse)


def convert_numbers_in_table(table):
    """
    Converts numbers in string form in all columns to Python numbers.

    Parameters:
    - table (list): The input table, where each element is a list representing a row.

    Returns:
    - list: The table with converted numeric values.
    """
    for row in table:
        for i in range(len(row)):
            try:
                row[i] = int(row[i])
            except ValueError:
                try:
                    row[i] = float(row[i])
                except ValueError:
                    pass  # Ignore if the conversion is not possible

    return table
