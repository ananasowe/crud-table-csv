import pandas as pd
import pandasql as ps


def run_query(table, columns, name, query):
    data = pd.DataFrame(table, columns=columns)
    result = ps.sqldf(query, locals())
    return result.values.tolist()
