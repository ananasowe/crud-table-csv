def remove_rows(table, column_num, column_value):
    filtered_out_table = []
    for row in table:
        if str(row[column_num]) != column_value:
            filtered_out_table.append(row)
    return filtered_out_table


def pick_rows(table, column_num, column_value):
    filtered_out_table = []
    for row in table:
        if str(row[column_num]) == column_value:
            filtered_out_table.append(row)
    return filtered_out_table
