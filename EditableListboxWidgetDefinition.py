from pygubu.api.v1 import BuilderObject, register_widget
from widgets.EditableListboxWidget import EditableListboxWidget


class EditableListboxWidgetBuilder(BuilderObject):
    class_ = EditableListboxWidget


register_widget(
    "EditableListboxWidgetDefinition.EditableListboxWidget",
    EditableListboxWidgetBuilder,
    "EditableListBoxWidget",
    ("tk", "Crud Table Browser widgets"),
)
