from tkinter import messagebox

import pygubu
from tksheet import Sheet
from ttkthemes import ThemedStyle

import DataLoader
import utils.filters
import utils.tables
import utils.query
from dialogs.AddColumnsDIalog import ColumnsDialog
from dialogs.FieldSelectDialog import FieldSelectDialog
from static_variables import PROJECT_PATH, PROJECT_UI


class CrudTableBrowser:
    def __init__(self, master=None, csv_data=None):
        # Zmienne globalne
        self.csv_data = csv_data
        self.swapped_headers = None
        self.sorted_header = None
        self.first_row_is_headers = False
        self.file_path = None

        # Inicjalizacja kreatora obiektów
        self.builder = builder = pygubu.Builder(on_first_object=self.on_first)
        builder.add_resource_path(PROJECT_PATH)

        # Ładowanie szablonu interfejsu
        builder.add_from_file(PROJECT_UI)

        # Główne okno
        self.mainwindow = builder.get_object('toplevel_mainwindow', master)

        self.mainwindow.geometry("800x600")
        self.frame_sheet = self.builder.get_object('frame_sheet')
        self.frame_sheet.grid_columnconfigure(0, weight=1)
        self.frame_sheet.grid_rowconfigure(0, weight=1)
        self.sheet = Sheet(self.frame_sheet,
                           data=self.csv_data,
                           align="w",
                           header_align="center",
                           row_index_align="w",
                           theme="light blue",
                           show_x_scrollbar=False,
                           width=800)
        self.sheet.enable_bindings(("single_select",
                                    "column_select",
                                    "row_select",
                                    "column_width_resize",
                                    "double_click_column_resize",
                                    "arrowkeys",
                                    "rc_select",
                                    "copy",
                                    "cut",
                                    "paste",
                                    "delete",
                                    "undo",
                                    "edit_cell"))
        self.sheet.extra_bindings([("cell_select", self.on_cell_select),
                                   ("column_select", self.on_column_select)])
        self.sheet.set_all_cell_sizes_to_text()
        self.sheet.set_options(auto_resize_columns=40)
        self.sheet.pack(expand=True, fill="both")

        self.mainwindow.bind("<Control-o>", self.on_command_open_clicked)
        self.mainwindow.bind("<Control-s>", self.on_command_save_clicked)
        self.mainwindow.bind("<Shift-Control-s>", self.on_command_saveas_clicked)
        self.mainwindow.bind("<Next>", self.on_command_next_page)
        self.mainwindow.bind("<Prior>", self.on_command_previous_page)
        # Aktywacja funkcji nasłuchujących zdarzeń interfejsu
        builder.connect_callbacks(self)

    def on_first(self, widget):
        style = ThemedStyle(widget)
        style.theme_use("vista")

    def run(self):
        self.mainwindow.mainloop()

    # Zdarzenia GUI

    def on_cell_select(self, event):
        print(event)

    def on_column_select(self, event):
        if len(self.sheet.data) == 0:
            messagebox.showwarning("Nie można sortować", "Brak wierszy")
            return
        column_id = event.column
        if self.sorted_header == column_id:
            self.sorted_header = None
            self.sort_column(column_id, reverse=True)
        else:
            self.sorted_header = column_id
            self.sort_column(column_id, reverse=False)
        print(event)

    def sort_column(self, column_id, reverse):
        self.csv_data = utils.tables.sort_table_by_column(self.csv_data, column_id, reverse=reverse)
        DataLoader.set_sheet_data(self, self.csv_data)

    def on_checkbox_first_row_as_name(self):
        toggled = DataLoader.toggle_header(self)
        print("First row as column name: ", toggled)

    def add_record(self, position):
        if len(self.sheet.data) == 0:
            popup = ColumnsDialog(self.mainwindow)
            popup_response = popup.run_waiting()
            if len(popup_response) > 0:
                result = [popup_response]
                DataLoader.set_sheet_data(self, result)
            return
        print("Inserting row below #", position)
        self.sheet.insert_row(idx=position, redraw=True)

    def on_button_add(self):
        if len(self.sheet.data) == 1 and self.first_row_is_headers:
            self.add_record(1)
            return
        row = self.sheet.get_currently_selected().row + 1 if self.sheet.get_currently_selected() \
            else len(self.sheet.data)
        self.add_record(row)

    def on_command_add_element_end(self):
        self.add_record(len(self.sheet.data))

    def on_command_add_element_start(self):
        row = 1 if len(self.sheet.data) == 1 and self.first_row_is_headers else 0
        self.add_record(row)

    def remove_selected(self):
        if len(self.sheet.data) == 0:
            messagebox.showwarning("Nie można usunąć wiersza", "Brak wierszy")
            return
        currently_selected = self.sheet.get_currently_selected()
        if not currently_selected:
            messagebox.showwarning("Nie można usunąć wiersza", "Nie zaznaczono wiersza do usunięcia")
            return
        row = currently_selected.row
        print("Removing row #", row)
        self.sheet.delete_row(idx=row, deselect_all=True, redraw=True)

    def on_button_remove(self):
        self.remove_selected()

    def on_command_remove_selected(self, *args):
        self.remove_selected()

    def on_command_remove_all_elements(self, *args):
        self.sheet.display_rows()  # Usuwa zaznaczenie
        self.sheet.set_sheet_data(data=[])  # Usuwa dane

    def on_command_remove_elements(self):
        if len(self.sheet.data) == 0:
            messagebox.showwarning("Nie można usunąć wiersza", "Brak wierszy")
            return
        filtered_out_table = self.perform_filtering(removing=True)
        if filtered_out_table:
            DataLoader.set_sheet_data(self, filtered_out_table)

    # Znajduje pasujące elementy i wyświetla w nowym widoku
    def on_command_search_elements(self, *args):
        if len(self.sheet.data) == 0:
            messagebox.showwarning("Nie można wyszukać wierszy", "Brak wierszy")
            return
        filtered_out_table = self.perform_filtering()
        if filtered_out_table:
            CrudTableBrowser(master=self.mainwindow, csv_data=filtered_out_table)

    def perform_filtering(self, removing=False):
        column_names = self.swapped_headers if self.first_row_is_headers else self.csv_data[0]
        selection = FieldSelectDialog(self.mainwindow, columns=column_names).run_waiting()
        if len(selection) == 0:
            return None
        field_name = selection[0]
        column_id = column_names.index(field_name)
        field_value = selection[1]
        if removing:
            remove_table = utils.filters.pick_rows(table=self.csv_data,
                                                         column_num=column_id, column_value=field_value)
            filtered_out_table = self.csv_data
            for row in remove_table:
                try:
                    self.csv_data.remove(row)
                except ValueError:
                    pass

        else:
            filtered_out_table = utils.filters.pick_rows(table=self.csv_data,
                                                         column_num=column_id, column_value=field_value)
        return filtered_out_table

    def on_button_sql_query(self, *args):
        if len(self.sheet.data) == 0:
            messagebox.showwarning("Nie można wyszukać wierszy", "Brak wierszy")
            return
        query = self.builder.get_variable('sql_query').get()
        self.execute_sql(query)

    def on_command_best_students(self, *args):
        if len(self.sheet.data) == 0 or self.swapped_headers != ['id', 'nazwisko', 'rok', 'przedmiot', 'ocena']:
            messagebox.showwarning("Nie można wyszukać studentów", "Nie załadowano tabeli studentów")
            return
        query = "SELECT nazwisko, przedmiot, AVG(ocena) FROM data GROUP BY nazwisko, przedmiot"
        self.execute_sql(query)

    def execute_sql(self, query):
        try:
            table = self.csv_data
            column_names = self.swapped_headers if self.first_row_is_headers else self.csv_data[0]
            result = utils.query.run_query(table, column_names, 'data', query)
            if result:
                CrudTableBrowser(master=self.mainwindow, csv_data=result)
            else:
                messagebox.showwarning("Brak wyników", "Kwerenda nie zwróciła odpowiedzi")
        except Exception as e:
            messagebox.showwarning("Błąd kwerendy", str(e))

    def scroll_to_next_page(self, forward=False):
        sheet_rows_num = len(self.sheet.data)
        if sheet_rows_num > 0:
            view_window_reached = False
            last_visible = 0
            search_range = range(0, sheet_rows_num) if forward else range(sheet_rows_num - 1, 0, -1)
            for row in search_range:
                cell_completely_visible = self.sheet.cell_completely_visible(r=row, c=0, seperate_axes=False)
                if cell_completely_visible:
                    last_visible = row
                    view_window_reached = True
                if not cell_completely_visible and view_window_reached:
                    break

            print("Moving view to cell #", last_visible)
            self.sheet.select_row(row=last_visible, redraw=False)
            self.sheet.see(row=last_visible, column=0, keep_yscroll=False, keep_xscroll=True,
                           bottom_right_corner=False, check_cell_visibility=True)

    def on_command_next_page(self, *args):
        self.scroll_to_next_page(forward=True)

    def on_command_previous_page(self, *args):
        self.scroll_to_next_page(forward=False)

    def on_command_to_start(self):
        if len(self.sheet.data) > 0:
            self.sheet.see(row=0, column=0, keep_yscroll=False, keep_xscroll=True,
                           bottom_right_corner=False, check_cell_visibility=True)

    def on_command_to_end(self):
        sheet_rows = len(self.sheet.data)
        if sheet_rows > 0:
            self.sheet.see(row=sheet_rows - 1, column=0, keep_yscroll=False, keep_xscroll=True,
                           bottom_right_corner=False, check_cell_visibility=True)

    def save_file(self, override):
        self.file_path = DataLoader.write_table_to_csv(self.sheet.data, self.file_path if override else None)

    def on_command_save_clicked(self, *param):
        self.save_file(override=True)

    def on_command_saveas_clicked(self, *param):
        self.save_file(override=False)

    def on_command_open_clicked(self, *param):
        self.file_path = DataLoader.load_csv(self)
        self.sheet.redraw()


if __name__ == '__main__':
    app = CrudTableBrowser()
    app.run()
